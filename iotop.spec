Summary: Top like utility for I/O       

Name: iotop
Version: 0.6
Release: 8%{?dist}
License: GPLv2+
URL: http://guichaz.free.fr/iotop/            
Source0: http://guichaz.free.fr/iotop/files/%{name}-%{version}.gita14256a3.tar.bz2

BuildRequires:	python3-devel gcc

%description
Linux has always been able to show how much I/O was going on
(the bi and bo columns of the vmstat 1 command).
iotop is a Python program with a top like UI used to
show of behalf of which process is the I/O going on.

%prep
%autosetup -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%files
%doc NEWS THANKS README ChangeLog
%license COPYING
%{python3_sitearch}/iotop*
%{_sbindir}/iotop
%{_mandir}/man8/iotop.*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-8
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-7
- Rebuilt for loongarch release

* Wed Sep 20 2023 Shuo Wang <abushwang@tencent.com> - 0.6-6
- Rebuilt for python 3.11

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.6-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Apr 15 2022 jackbertluo <jackbertluo@tencent.com> - 0.6-1 
- Initial build
